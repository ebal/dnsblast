# DNSBlast

> a simple and really stupid load testing tool for DNS resolvers

https://github.com/jedisct1/dnsblast

## Download

Download **dnsblast** binary from here: **[dnsblast](https://gitlab.com/ebal/dnsblast/-/jobs/artifacts/main/raw/dnsblast?job=run-build)**

## Install

```bash
$ curl -sLo dnsblast https://gitlab.com/ebal/dnsblast/-/jobs/artifacts/main/raw/dnsblast?job=run-build

$ chmod +x dnsblast

$ ./dnsblast
Usage: dnsblast [fuzz] <host> [<count>] [<pps>] [<port>]

```

## Run

eg.

```bash
$ dnsblast 88.198.92.222 30 10
Sent: [30] - Received: [30] - Reply rate: [10 pps] - Ratio: [100.00%]

$ dnsblast 88.198.92.222 40 10
Sent: [40] - Received: [35] - Reply rate: [9 pps] - Ratio: [87.50%]

```
